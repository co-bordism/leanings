def main : IO Unit :=
  IO.println "Hello, world!"


namespace SimpleTypes
  /- Declare some constants. -/
  constant m  : Nat   -- m is a natural number
  constant n  : Nat
  constant b1 : Bool  -- b1 is a Boolean
  constant b2 : Bool

  /- Check their types. -/
  #check m            -- output: Nat
  #check n
  #check n + 0        -- Nat
  #check m * (n + 0)  -- Nat
  #check b1           -- Bool
  #check b1 && b2     -- "&&" is the Boolean and
  #check b1 || b2     -- Boolean or
  #check true         -- Boolean "true"



  constant f  : Nat → Nat         -- type the arrow as "\to" or "\r"
  constant f' : Nat -> Nat        -- alternative ASCII notation
  constant p  : Nat × Nat         -- type the product as "\times"
  constant q  : Prod Nat Nat      -- alternative notation
  constant g  : Nat → Nat → Nat
  constant g' : Nat → (Nat → Nat) -- has the same type as g!
  constant h  : Nat × Nat → Nat
  constant F  : (Nat → Nat) → Nat -- a "functional"

  #check f            -- Nat → Nat
  #check f n          -- Nat
  #check g m n        -- Nat
  #check g m          -- Nat → Nat
  #check (m, n)       -- Nat × Nat
  #check p.1          -- Nat
  #check p.2          -- Nat
  #check (m, n).1     -- Nat
  #check (p.1, n)     -- Nat × Nat
  #check F f          -- Nat
end SimpleTypes


namespace TypesAsObjects
  #check Nat               -- Type
  #check Bool              -- Type
  #check Nat → Bool        -- Type
  #check Nat × Bool        -- Type
  #check Nat → Nat         -- ...
  #check Nat × Nat → Nat
  #check Nat → Nat → Nat
  #check Nat → (Nat → Nat)
  #check Nat → Nat → Bool
  #check (Nat → Nat) → Nat

  constant α : Type
  constant β : Type
  constant F : Type → Type
  constant G : Type → Type → Type

  #check α        -- Type
  #check F α      -- Type
  #check F Nat    -- Type
  #check G α      -- Type → Type
  #check G α β    -- Type
  #check G α Nat  -- Type

  #check Prod α β       -- Type
  #check Prod Nat Nat   -- Type
  #check List α    -- Type
  #check List Nat  -- Type

  #check Type     -- Type 1
  #check Type 1   -- Type 2
  #check Type 2   -- Type 3
  #check Type 3   -- Type 4
  #check Type 32   
  #check Prop
  #check List
  #check Prod

  universe u
  constant α' : Type u
  #check α'

  constant α'' : Type _
  #check α''

  -- alterante Type* notation from Lean 3 as macro
  macro "Type*" : term => `(Type _)
  def f (α : Type*) (a : α) := a
  def g (α : Type _) (a : α) := a
  #check f
  #check g
end TypesAsObjects

namespace functions
  namespace basics
    #check fun (x : Nat) => x + 5
    #check λ (x : Nat) => x + 5
    #check fun x : Nat => x + 5
    #check λ x : Nat => x + 5

    constant f : Nat → Nat
    constant h : Nat → Bool → Nat

    #check fun x : Nat => fun y : Bool => h (f x) y   -- Nat → Bool → Nat
    #check fun (x : Nat) (y : Bool) => h (f x) y      -- Nat → Bool → Nat
    #check fun x y => h (f x) y  
  end basics
  namespace examples
    constant f : Nat → String
    constant g : String → Bool
    constant b : Bool

    #check fun x : Nat => x        -- Nat → Nat
    #check fun x : Nat => b        -- Nat → Bool
    #check fun x : Nat => g (f x)  -- Nat → Bool
    #check fun x => g (f x)        -- Nat → Bool

    #check fun (g : String → Bool) (f : Nat → String) (x : Nat) => g (f x)
    #check fun (α β γ : Type) (g : β → γ) (f : α → β) (x : α) => g (f x)
  end examples

  namespace application
    #check (fun x : Nat => x) 1     -- Nat
    #check (fun x : Nat => true) 1  -- Bool

    constant f : Nat → String
    constant g : String → Bool

    #check
      (fun (α β γ : Type) (g : β → γ) (f : α → β) (x : α) => g (f x)) Nat String Bool g f 0


      #reduce (fun x : Nat => x) 1     -- 1
    #reduce (fun x : Nat => true) 1  -- true

    #reduce
      (fun (α β γ : Type) (g : β → γ) (f : α → β) (x : α) => g (f x)) Nat String Bool g f 0
      -- g (f 0)
      -- Bool
  end application
  namespace examplesOfreduce
    constant m : Nat
    constant n : Nat
    constant b : Bool

    #reduce (m, n).1        -- m
    #reduce (m, n).2        -- n

    #reduce true && false   -- false
    #reduce false && b      -- false
    #reduce b && false      -- Bool.rec false false b

    #reduce n + 0           -- n
    #reduce n + 2           -- Nat.succ (Nat.succ n)
    #reduce 2 + 3           -- 5
  end examplesOfreduce
end functions


namespace definitions
  def foo : (Nat → Nat) → Nat :=
    fun f => f 0

  #check foo   -- (Nat → Nat) → Nat
  #print foo

  namespace notations
    def double (x : Nat) : Nat :=
      x + x

    #print double
    #check double 3
    #reduce double 3  -- 6
    #eval double 3    -- 6

    def square (x : Nat) :=
      x * x

    #print square
    #check square 3
    #reduce square 3  -- 9
    #eval square 3    -- 9

    def doTwice (f : Nat → Nat) (x : Nat) : Nat :=
      f (f x)

    #eval doTwice double 2   -- 8

    -- These definitions are equivalent to the following:


    def double' : Nat → Nat :=
      fun x => x + x

    def square' : Nat → Nat :=
      fun x => x * x

    def doTwice' : (Nat → Nat) → Nat → Nat :=
      fun f x => f (f x)

    def doTwice'' : (Nat -> Nat) → Nat → Nat :=
      λ f n => f ( f n )

    def compose (α β γ : Type) (g : β → γ) (f : α → β) (x : α) : γ :=
        g (f x)

    def compose' (α β γ : Type) (g : β → γ) (f : α → β) : α → γ :=
        λ x => g (f x)
    
    def compose''' (α β γ : Type) (g : β → γ) :  (f: α → β) →  α → γ :=
        λ h x => g (h x) --- should be f, not h. ^^^
  end notations
end definitions

namespace dependent
  #check @List.cons    -- {α : Type u_1} → α → List α → List α
  #check @List.nil     -- {α : Type u_1} → List α
  #check List.nil
  #check (List.nil : List Nat)
  #check @List.length  -- {α : Type u_1} → List α → Nat
  #check @List.append  -- {α : Type u_1} → List α → List α → List α

  universes u v

  def f (α : Type u) (β : α → Type v) (a : α) (b : β a) : (a : α) × β a :=
    ⟨a, b⟩
  def f' : (α: Type u) → (β : α → Type v) → (a: α) → β a → α × (β a) :=
    λ α β a b => (a,b)

  def g (α : Type u) (β : α → Type v) (a : α) (b : β a) : Σ a : α, β a :=
    Sigma.mk a b

  #reduce f
  #reduce g

  #reduce f Type (fun α => α) Nat 10
  #reduce g Type (fun α => α) Nat 10

  #reduce (f Type (fun α => α) Nat 10).1 -- Nat
  #reduce (g Type (fun α => α) Nat 10).1 -- Nat
  #reduce (f Type (fun α => α) Nat 10).2 -- 10
  #reduce (g Type (fun α => α) Nat 10).2 -- 10
end dependent

namespace Organising
  namespace variablesAndSections
    def compose (α β γ : Type) (g : β → γ) (f : α → β) (x : α) : γ :=
      g (f x)

    def doTwice (α : Type) (h : α → α) (x : α) : α :=
      h (h x)

    def doThrice (α : Type) (h : α → α) (x : α) : α :=
      h (h (h x))

    -- using variables 
    variable (α β γ : Type)

    def compose' (g : β → γ) (f : α → β) (x : α) : γ :=
      g (f x)

    def doTwice' (h : α → α) (x : α) : α :=
      h (h x)

    def doThrice' (h : α → α) (x : α) : α :=
      h (h (h x))

    -- using more variables
    variable (α β γ : Type)
    variable (g : β → γ) (f : α → β) (h : α → α)
    variable (x : α)

    def compose'' := g (f x)
    def doTwice'' := h (h x)
    def doThrice'' := h (h (h x))

    #print compose''
    #print doTwice''
    #print doThrice''

    -- now with limited scope:
    section useful
      variable (α β γ : Type)
      variable (g : β → γ) (f : α → β) (h : α → α)
      variable (x : α)

      def compose3 := g (f x)
      def doTwice3 := h (h x)
      def doThrice3 := h (h (h x))
    end useful
  end variablesAndSections
  namespace namespaces
    namespace Foo
      def a : Nat := 5
      def f (x : Nat) : Nat := x + 7

      def fa : Nat := f a
      def ffa : Nat := f (f a)

      #check a
      #check f
      #check fa
      #check ffa
      #check Foo.fa
    end Foo

    -- #check a  -- error
    -- #check f  -- error
    #check Foo.a
    #check Foo.f
    #check Foo.fa
    #check Foo.ffa

    open Foo

    #check a
    #check f
    #check fa
    #check Foo.fa

    #check List.nil
    #check List.cons
    #check List.map


    open List

    #check nil
    #check cons
    #check map

    namespace Foo
      -- def a : Nat := 5
      -- def f (x : Nat) : Nat := x + 7

      -- def fa : Nat := f a

      namespace Bar
        def ffa : Nat := f (f a)

        #check fa
        #check ffa
      end Bar

      #check fa
      #check Bar.ffa
    end Foo

    #check Foo.fa
    #check Foo.Bar.ffa

    open Foo

    #check fa
    #check Bar.ffa

    namespace Foo
      -- def a : Nat := 5
      -- def f (x : Nat) : Nat := x + 7

      -- def fa : Nat := f a
    end Foo

    #check Foo.a
    #check Foo.f

    namespace Foo
      -- def ffa : Nat := f (f a)
    end Foo
  end namespaces

  namespace implicits
    -- Implicit Arguments
    -- Suppose we define the compose function as.
    def compose (α β γ : Type) (g : β → γ) (f : α → β) (x : α) : γ :=
      g (f x)
    -- The function compose takes three types, α, β, and γ, and two functions, g : β → γ and f : α → β, a value x : α, and returns g (f x), the composition of g and f. We say compose is polymorphic over types α, β, and γ. Now, let's use compose:

    def double (x : Nat) := 2*x
    def triple (x : Nat) := 3*x

    #check compose Nat Nat Nat double triple 10 -- Nat
    #eval  compose Nat Nat Nat double triple 10 -- 60

    def appendWorld (s : String) := s ++ "world"
    #check String.length -- String → Nat

    #check compose String String Nat String.length appendWorld "hello" -- Nat
    #eval  compose String String Nat String.length appendWorld "hello" -- 10
    -- Because compose is polymorphic over types α, β, and γ, we have to provide them in the examples above. But this information is redundant: one can infer the types from the arguments g and f. This is a central feature of dependent type theory: terms carry a lot of information, and often some of that information can be inferred from the context. In Lean, one uses an underscore, _, to specify that the system should fill in the information automatically.


    #check compose _ _ _ double triple 10 -- Nat
    #eval  compose Nat Nat Nat double triple 10 -- 60
    #check compose _ _ _ String.length appendWorld "hello" -- Nat
    #eval  compose _ _ _ String.length appendWorld "hello" -- 10
    -- It is still tedious, however, to type all these underscores. When a function takes an argument that can generally be inferred from context, Lean allows us to specify that this argument should, by default, be left implicit. This is done by putting the arguments in curly braces, as follows:


    def compose' {α β γ : Type} (g : β → γ) (f : α → β) (x : α) : γ :=
      g (f x)
    def compose2 : {α : Type} → {β : Type} -> {γ : Type} → (g : β → γ)→ (f : α → β) → (x : α) → γ :=
      fun g f x => g ( f x)
    #check compose' double triple 10 -- Nat
    #eval  compose' double triple 10 -- 60
    #check compose' String.length appendWorld "hello" -- Nat
    #eval  compose' String.length appendWorld "hello" -- 10
    -- All that has changed are the braces around α β γ: Type. It makes these three arguments implicit. Notationally, this hides the specification of the type, making it look as though compose simply takes 3 arguments.

    -- Variables can also be specified as implicit when they are declared with the variables command:
    universe u

    section
      variable {α : Type u}
      variable (x : α)
      def ident := x
      #check (@ident Nat)
    end

    variable (α β : Type u)
    variable (a : α) (b : β)

    #check ident
    #check ident a
    #check ident b
    -- This definition of ident here has the same effect as the one above.

    -- Lean has very complex mechanisms for instantiating implicit arguments, and we will see that they can be used to infer function types, predicates, and even proofs. The process of instantiating these "holes," or "placeholders," in a term is part of a bigger process called elaboration. The presence of implicit arguments means that at times there may be insufficient information to fix the meaning of an expression precisely. An expression like ident is said to be polymorphic, because it can take on different meanings in different contexts.

    -- One can always specify the type T of an expression e by writing (e : T). This instructs Lean's elaborator to use the value T as the type of e when trying to elaborate it. In the following example, this mechanism is used to specify the desired types of the expressions ident.


    def ident' {α : Type u} (a : α) : α := a

    #check (ident' : Nat → Nat) -- Nat → Nat

    -- Numerals are overloaded in Lean, but when the type of a numeral cannot be inferred, Lean assumes, by default, that it is a natural number. So the expressions in the first two #check commands below are elaborated in the same way, whereas the third #check command interprets 2 as an integer.


    #check 2         -- Nat
    #check (2 : Nat) -- Nat
    #check (2 : Int) -- Int

    -- Sometimes, however, we may find ourselves in a situation where we have declared an argument to a function to be implicit, but now want to provide the argument explicitly. If foo is such a function, the notation @foo denotes the same function with all the arguments made explicit.


    variable (α β : Type)

    #check @ident           -- {α : Type u_1} → α → α
    #check @ident α         -- α → α
    #check @ident β         -- β → β
    #check @ident Nat       -- Nat → Nat
    #check @ident Bool true -- Bool

    -- Notice that now the first #check command gives the type of the identifier, ident, without inserting any placeholders. Moreover, the output indicates that the first argument is implicit.

    -- Named arguments enable you to specify an argument for a parameter by matching the argument with its name rather than with its position in the parameter list. You can use them to specify explicit and implicit arguments. If you don't remember the order of the parameters but know their names, you can send the arguments in any order. You may also provide the value for an implicit parameter when Lean failed to infer it. Named arguments also improve the readability of your code by identifying what each argument represents.



    #check ident (α := Nat)  -- Nat → Nat
    #check ident (α := Bool) -- Bool → Bool
    #check compose2 (g := (ident: Nat → Nat)) (α := Nat × Nat)
  end implicits
  namespace autoimplicit
    -- In the previous section, we have shown how implicit arguments make functions more convenient to use. However, functions such as compose are still quite verbose to define. Note that the universe polymorphic compose is even more verbose than the one previously defined.

    universes u v w
    def compose {α : Type u} {β : Type v} {γ : Type w}
                (g : β → γ) (f : α → β) (x : α) : γ :=
      g (f x)
    -- You can avoid the universe command by providing the universe parameters when defining compose.


    def compose2.{u2, v2, w2}
                {α : Type u2} {β : Type v2} {γ : Type w2}
                (g : β → γ) (f : α → β) (x : α) : γ :=
      g (f x)
    -- Lean 4 supports a new feature called auto bound implicit arguments. It makes functions such as compose much more convenient to write. When Lean processes the header of a declaration, any unbound identifier is automatically added as an implicit argument if it is a single lower case or greek letter. With this feature, we can write compose as


    def compose3 (g : β → γ) (f : α → β) (x : α) : γ :=
      g (f x)

    #check @compose3
    -- {β : Sort u_1} → {γ : Sort u_2} → {α : Sort u_3} → (β → γ) → (α → β) → α → γ
    -- Note that, Lean inferred a more general type using Sort instead of Type.

    -- Although we love this feature and use it extensively when implementing Lean, we realize some users may feel uncomfortable with it. Thus, you can disable it using the command set_option autoBoundImplicitLocal false.


    -- set_option autoBoundImplicitLocal false
    /- The following definition produces `unknown identifier` errors -/
    -- def compose (g : β → γ) (f : α → β) (x : α) : γ :=
    --   g (f x)

  end autoimplicit
end Organising

namespace declaringTypes
  inductive Weekday where 
    | Sunday 
    | Monday 
    | Tuesday  -- three day week :)

  def toString' : ( d: Weekday ) -> String  :=
    fun d => 
    match d with
    | Weekday.Sunday    => "Sunday"
    | Weekday.Monday => "Monday"
    | Weekday.Tuesday   => "Tuesday"

  #eval (toString' Weekday.Monday)
  namespace Weekday
    def toNum : (d : Weekday) -> Nat 
      | Sunday => 7
      | Monday => 1
      | Tuesday => 2

    #eval (toNum Monday)
  end Weekday
  
  namespace structures
    structure TwoDPoint (α : Type u) where 
      x_coord : α 
      y_coord : α  
    
    #check TwoDPoint  -- Type u -> Type u
    #check @TwoDPoint.mk   -- {α : Type u_1} → α → α → TwoDPoint α
    #check @TwoDPoint.x_coord    -- {α : Type u} → TwoDPoint α → α
    #check @TwoDPoint.y_coord    -- {α : Type u} → TwoDPoint α → α

    #check TwoDPoint.mk 10 20                   -- Point Nat
    #check { x_coord := 10, y_coord := 20 : TwoDPoint Nat } -- Point Nat

    def mkTwoDPoint (a : Nat) : TwoDPoint Nat :=
      { x_coord := a, y_coord := a }

    -- #eval (Point.mk 10 20).x                   -- 10
    -- #eval (Point.mk 10 20).y                   -- 20
    -- #eval { x := 10, y := 20 : Point Nat }.x   -- 10
    -- #eval { x := 10, y := 20 : Point Nat }.y   -- 20

    structure Point (α : Type u) where
      x : α
      y : α

    def mkPoint (a : Nat) : Point Nat where
      x := a
      y := a

    -- Dot notation:
    def Point.add (p q : Point Nat) : Point Nat :=
      { x := p.x + q.x, y := p.y + q.y }

    def p : Point Nat := Point.mk 1 2
    def q : Point Nat := {x:=3 , y:=4}

    #eval (p.add q).x  -- 4
    #eval (p.add q).y  -- 6

    def Point.smul (n : Nat) (p : Point Nat) :=
      -- Point.mk (n * p.x) (n * p.y)
      {x := n*p.x, y :=n*p.y : Point Nat}

    #eval (p.smul 3).x -- 3
    #eval (p.smul 3).y -- 6

    namespace inheritance
      structure Point (α : Type u) where
        x : α
        y : α

      inductive Color where
        | red
        | green
        | blue

      structure ColorPoint (α : Type u) extends Point α where
        color : Color

      #check { x := 10, y := 20, color := Color.red : ColorPoint Nat }
      -- { toPoint := { x := 10, y := 20 }, color := Color.red }

      structure Foo where
        x : Nat
        y : Nat

      structure Boo where
        w : Nat
        z : Nat

      structure Bla extends Foo, Boo where
        bit : Bool

      #check Bla.mk -- Foo → Boo → Bool → Bla
      #check Bla.mk { x := 10, y := 20 } { w := 30, z := 40 } true
      #check { x := 10, y := 20, w := 30, z := 40, bit := true : Bla }
      #check { toFoo := { x := 10, y := 20 }, toBoo := { w := 30, z := 40 }, bit := true : Bla }

      def j := Bla.mk { x := 10, y := 20 } { w := 30, z := 40 } true
      #check j.x
      #check j.toFoo.x

      theorem ex :
          Bla.mk { x := x, y := y } { w := w, z := z } b
          =
          { x := x, y := y, w := w, z := z, bit := b } :=
        rfl

    end inheritance
    namespace defaults
      -- You can assign default value to fields when declaring a new structure.
      inductive MessageSeverity
        | error | warning

      structure Message where
        fileName : String
        pos      : Option Nat      := none
        severity : MessageSeverity := MessageSeverity.error
        caption  : String          := ""
        data     : String

      def msg1 : Message :=
        { fileName := "foo.lean", data := "failed to import file" }

      #eval msg1.pos      -- none
      #eval msg1.fileName -- "foo.lean"
      #eval msg1.caption  -- ""

      -- When extending a structure, you can not only add new fields, but provide new default values for existing fields.

      structure MessageExt extends Message where
        timestamp : Nat
        caption   := "extended" -- new default value for field `caption`

      def msg2 : MessageExt where
        fileName  := "bar.lean"
        data      := "error at initialization"
        timestamp := 10

      #eval msg2.fileName  -- "bar.lean"
      #eval msg2.timestamp -- 10
      #eval msg2.caption   -- "extended" 
    end defaults
  end structures
  namespace typeclasses
    namespace asStructure
      structure Add (a : Type) where
        add : a → a → a
      #check @Add.add -- Add.add : {A : Type} → Add A → {a : Sort u_1} → a → a → a
      
      def double (s : Add a) (x : a) : a :=
        s.add x x

      #eval double { add := Nat.add } 10
      -- 20

      #eval double { add := Nat.mul } 10
      -- 100

      #eval double { add := Int.add } 10
      -- 20
    end asStructure
    namespace asClass
      class Add (a : Type) where
        add : a → a → a
      #check @Add.add -- Add.add : {a : Type} → [self : Add a] → a → a → a
      -- This version of add is the Lean analogue of the Haskell term add :: Add a => a -> a -> a
      
      instance : Add Nat where
        add := Nat.add
      
      instance [Add a] : Add (Array a) where
        add x y := Array.zipWith x y (fun x_1 x_2 => Add.add x_1 x_2)

      #eval Add.add #[1, 2] #[3, 4]  -- #[4, 6]

      -- #eval #[1, 2] + #[3, 4]  -- #[4, 6]   -- looks like "+" uses hAdd instead of add.
    end asClass
    namespace nonEmpty
      class Inhabited (a : Type u) where
        default : a
      #check @Inhabited.default -- {a : Type u_1} → [self : Inhabited a] → a

      instance : Inhabited Bool where
        default := true

      instance : Inhabited Nat where
        default := 0

      instance : Inhabited Unit where
        default := ()

      instance : Inhabited Prop where
        default := True
      #eval (Inhabited.default : Nat)
      -- 0

      #eval (Inhabited.default : Bool)
      -- true

      export Inhabited (default)

      #eval (default : Nat)
      -- 0

      #eval (default : Bool)
      -- true

      -- Arbitrary is an 'opaque' version of default.. 

      constant arbitrary [Inhabited a] : a :=
        Inhabited.default
      
      instance [Inhabited a] [Inhabited b] : Inhabited (a × b) where
        default := (arbitrary, arbitrary)

      -- With this added to the earlier instance declarations, type class instance can infer, for example, a default element of Nat × Bool:

      instance [Inhabited a] [Inhabited b] : Inhabited (a × b) where
        default := (arbitrary, arbitrary) -- why not default? 

      #eval (arbitrary : Nat × Bool)
      -- (0, true)
      #eval (default : Nat × Bool)
      -- (0, true)

      -- function type:
      instance [Inhabited b] : Inhabited (a -> b) where
        default := fun _ => arbitrary
      
      --try for List and Sum
      #check List -- Type u_1 → Type u_1
      instance [Inhabited a] : Inhabited (List a) where
        default := [arbitrary]

      #check Sum -- Type u_1 → Type u_2 → Type (max u_1 u_2)
      instance [Inhabited a] : Inhabited (Sum a b) where
        default := Sum.inl arbitrary
      instance [Inhabited b] : Inhabited (Sum a b) where
        default := Sum.inr arbitrary
      #check @inferInstance  -- {α : Type u} → [i : α] → α

      #check (inferInstance : Inhabited Nat) -- Inhabited Nat
      #check Inhabited
      #check (Inhabited Nat)
      def foo : Inhabited (Nat × Nat) :=
        inferInstance
      theorem ex : foo.default = (arbitrary, arbitrary) :=
        rfl
      #print inferInstance
    end nonEmpty
    namespace toString2
      -- The polymorphic method toString has type {α : Type u} → [ToString α] → α → String.
      structure Person where
        name : String
        age  : Nat

      -- class ToString (a : Type u) where
      --   toString : a → String

      instance : ToString Person where
        toString p := p.name ++ "@" ++ toString p.age

      #eval toString { name := "Leo", age := 542 : Person }
      #eval toString ({ name := "Daniel", age := 18 : Person }, "hello")
    end toString2
    namespace Numerals
      structure Rational where
        num : Int
        den : Int
        inv : den ≠ 0
      #print OfNat
      -- instance : OfNat Rational n where
      --   ofNat := { num := n, den := 1, inv := by decide}
     
      instance : ToString Rational where
        toString r := s!"{r.num}/{r.den}"

      -- #eval (2 : Rational) -- 2/1

      -- #check (2 : Rational) -- Rational
      #check (2 : Nat)      -- Nat


      class Monoid (α : Type u) where
        unit : α
        op   : α → α → α

      #check nat_lit 1
      -- instance [s : Monoid α] : OfNat α (nat_lit 1) where
      --   ofNat := s.unit

      def getUnit [Monoid α] : α :=
        1
    end Numerals
    namespace outputparameters
      class HMul (α : Type u) (β : Type v) (γ : outParam (Type w)) where
        hMul : α → β → γ

      export HMul (hMul)

      instance : HMul Nat Nat Nat where
        hMul := Nat.mul

      instance : HMul Nat (Array Nat) (Array Nat) where
        hMul a bs := bs.map (fun b => hMul a b)

      #eval hMul 4 3           -- 12
      #eval hMul 4 #[2, 3, 4]  -- #[8, 12, 16]

      -- This is done to say that α and β are enough to satart synthesizing the typeclass. The γ will be clear 

    -- what happens if I leave it out?
      class HMul' (α : Type u) (β : Type v) (γ : (Type w)) where
        hMul : α → β → γ
      instance : HMul' Nat Nat Nat where
        hMul := Nat.mul
        -- #eval HMul'.hMul (4: Nat) (3: Nat)  -- failed to synthesize instance HMul' Nat Nat ?m.16392
        -- #eval @HMul'.hMul Nat Nat Nat (____) (3: Nat) (4: Nat)
        #check @HMul'.hMul -- HMul'.hMul : {α : Type u_1} → {β : Type u_2} → {γ : Type u_3} → [self : HMul' α β γ] → α → β → γ
        -- If we now wanted to make hMul 3 4, then what would γ be? On the other hand, if γ is an output, I assume we can only have one hMul starting with Nat Nat.
        instance : HMul' Nat Nat Int where 
          hMul x y := -3
        -- #eval HMul'.hMul 3 4 -- typeclass instance problem is stuck, it is often due to metavariables   HMul' Nat Nat ?m.16442
        -- fine. I guess... ?
        instance : HMul Nat Nat Int where
          hMul x y := -3
        -- #eval HMul'.hMul 3 4 : Int  -- makes sense - does not know if gamma is Nat or Int...


        instance : HMul Int Int Int where
          hMul := Int.mul

        instance [HMul α β γ] : HMul α (Array β) (Array γ) where
          hMul a bs := bs.map (fun b => hMul a b)

        #eval hMul 4 3                    -- 12
        #eval hMul 4 #[2, 3, 4]           -- #[8, 12, 16]
        #eval hMul (-2) #[3, -1, 4]       -- #[-6, 2, -4]
        #eval hMul 2 #[#[2, 3], #[0, 4]]  -- #[#[4, 6], #[0, 8]]
    end outputparameters
    namespace defaultInstances
      class HMul (α : Type u) (β : Type v) (γ : outParam (Type w)) where
        hMul : α → β → γ

      export HMul (hMul)

      @[defaultInstance]
      instance : HMul Int Int Int where
        hMul := Int.mul

      def xs : List Int := [1, 2, 3]

      #check fun y => xs.map (fun x => hMul x y)  -- Int -> List Int
      
      -- let me try that with my nat to int..
      class HMul' (α : Type u) (β : Type v) (γ : outParam (Type w)) where
        hMul : α → β → γ

      instance : HMul' Nat Nat Nat where
        hMul := Nat.mul

      @[defaultInstance]
      instance : HMul' Nat Nat Int where 
        hMul x y := -3

      #eval HMul'.hMul 4 5 -- -3
      -- but how do I specify when I want the other one?
      -- #check_failure @HMul'.hMul Nat Nat Nat _ 3 4 
        -- failed to synthesize instance
        -- HMul' Nat Nat Nat
        -- ???
      -- #check @HMul'.hMul Nat Nat Nat Nat.mul 3 4 
      -- I'll ask on Zulip.
    end defaultInstances
  end typeclasses
end declaringTypes
namespace builtinTypes
  namespace arrays
    #eval #['a', 'b', 'c'][1]
    -- 'b'

    def getThird (xs : Array Nat) : Nat :=
      xs[2]

    #eval getThird #[10, 20, 30, 40]
    -- 30
    def f (xs : List Nat) : List Nat :=
      xs ++ xs

    def as : Array Nat :=
      #[1, 2, 3, 4]

    #eval f [1, 2, 3] -- This is a function application

    #eval as[2] -- This is an array access
  end arrays
end builtinTypes
namespace functions
  def fact x :=
    match x with
    | 0   => 1
    | n+1 => (n+1) * fact n

  #eval fact 100

  partial def g (x : Nat) (p : Nat -> Bool) : Nat :=
    if p x then
      x
    else
      g (x+1) p

  #eval g 0 (fun x => x > 10)

  def twice (f : Nat -> Nat) (x : Nat) : Nat :=
    f (f x)

  #eval twice (fun x => x + 1) 3
  #eval twice (fun (x : Nat) => x * 2) 3

  #eval List.map (fun x => x + 1) [1, 2, 3]
  -- [2, 3, 4]

  #eval List.map (fun (x, y) => x + y) [(1, 2), (3, 4)]
  -- [3, 7]


  #check (. + 1)
  -- fun a => a + 1
  #check (· + 1)
  -- fun a => a + 1
  #check (2 - .)
  -- fun a => 2 - a
  #eval [1, 2, 3, 4, 5].foldl (. * .) 1
  -- 120

  def h (x y z : Nat) :=
    x + y + z

  #check (h . 1 .)
  -- fun a b => h a 1 b

  #eval [(1, 2), (3, 4), (5, 6)].map (·.1)
  -- [1, 3, 5]

  namespace pipelining
    -- Pipelining enables function calls to be chained together as successive operations. Pipelining works as follows:

    def add1 x := x + 1
    def times2 x := x * 2

    #eval times2 (add1 100)
    #eval 100 |> add1 |> times2
    #eval times2 <| add1 <| 100

    -- The result of the previous #eval commands is 202. The forward pipeline |> operator takes a function and an argument and return a value. In contrast, the backward pipeline <| operator takes an argument and a function and returns a value. These operators are useful for minimizing the number of parentheses.


    def add1Times3FilterEven (xs : List Nat) :=
      List.filter (. % 2 == 0) (List.map (. * 3) (List.map (. + 1) xs))

    #eval add1Times3FilterEven [1, 2, 3, 4]
    -- [6, 12]

    -- Define the same function using pipes
    def add1Times3FilterEven' (xs : List Nat) :=
      xs |> List.map (. + 1) |> List.map (. * 3) |> List.filter (. % 2 == 0)

    #eval add1Times3FilterEven' [1, 2, 3, 4]
    -- [6, 12]

    -- Lean also supports the operator |>. which combines forward pipeline |> operator with the . field notation.

    -- Define the same function using pipes
    def add1Times3FilterEven'' (xs : List Nat) :=
      xs.map (. + 1) |>.map (. * 3) |>.filter (. % 2 == 0)

    #eval add1Times3FilterEven'' [1, 2, 3, 4]
    -- [6, 12]
    -- For users familiar with the Haskell programming language, Lean also supports the notation f $ a for the backward pipeline f <| a.
  end pipelining
end functions


namespace tactics
  --  The keyword by instructs Lean to use the tactic DSL to construct a term
  theorem ex1 : p ∨ q → q ∨ p := by
  -- Our initial goal is a hole with type p ∨ q → q ∨ p. The tactic intro h fills this hole using the term fun h => ?m where ?m is a new hole we need to solve.
  intro h
  -- This hole has type q ∨ p, and the local context contains h : p ∨ q. The tactic cases fills the hole using Or.casesOn h (fun h1 => ?m1) (fun h2 => ?m2) where ?m1 and ?m2 are new holes. 
  cases h with
  | inl h1 =>
    -- The tactic apply Or.inr fills the hole ?m1 with the application Or.inr ?m3, and exact h1 fills ?m3 with h1. 
    apply Or.inr
    exact h1
  | inr h2 =>
    apply Or.inl
    --  The tactic assumption tries to fill a hole by searching the local context for a term with the same type.
    assumption

  #print ex1
  /-
  theorem ex1 : {p q : Prop} → p ∨ q → q ∨ p :=
  fun {p q : Prop} (h : p ∨ q) =>
    Or.casesOn h (fun (h1 : p) => Or.inr h1) fun (h2 : q) => Or.inl h2
  -/

  -- You can use `match-with` in tactics.
  theorem ex2 : p ∨ q → q ∨ p := by
    intro h
    match h with
    | Or.inl _  => apply Or.inr; assumption
    | Or.inr h2 => apply Or.inl; exact h2
  
  -- As we have the `fun+match` syntax sugar for terms,
  -- we have the `intro+match` syntax sugar
  theorem ex3 : p ∨ q → q ∨ p := by
    intro
    | Or.inl h1 =>
      apply Or.inr
      exact h1
    | Or.inr h2 =>
      apply Or.inl
      assumption

  theorem ex1' : p ∨ q → q ∨ p := by
    intro h
    cases h
    apply Or.inr
    assumption
    apply Or.inl
    assumption
    done -- fails with an error here if there are unsolvable goals
  
  theorem ex2' : p ∨ q → q ∨ p := by
    intro h
    cases h
    focus -- instructs Lean to `focus` on the first goal,
      apply Or.inr
      assumption
      -- it will fail if there are still unsolvable goals here
    focus
      apply Or.inl
      assumption

  theorem ex3' : p ∨ q → q ∨ p := by
    intro h
    cases h
    -- You can still use curly braces and semicolons instead of
    -- whitespace sensitive notation as in the previous example
    { apply Or.inr;
      assumption
      -- It will fail if there are unsolved goals
    }
    { apply Or.inl;
      assumption
    }

  -- Many tactics tag subgoals. The tactic `cases` tag goals using constructor names.
  -- The tactic `case tag => tactics` instructs Lean to solve the goal
  -- with the matching tag.
  theorem ex4 : p ∨ q → q ∨ p := by
    intro h
    cases h
    case inr =>
      apply Or.inl
      assumption
    case inl =>
      apply Or.inr
      assumption
  


  -- As a convenience, pattern-matching has been integrated into tactics such as intro and funext.
  theorem ex1b : s ∧ q ∧ r → p ∧ r → q ∧ p := by
    intro ⟨_, ⟨hq, _⟩⟩ ⟨hp, _⟩
    exact ⟨hq, hp⟩

  theorem ex2b :
      (fun (x : Nat × Nat) (y : Nat × Nat) => x.1 + y.2)
      =
      (fun (x : Nat × Nat) (z : Nat × Nat) => z.2 + x.1) := by
    funext (a, b) (c, d)
    show a + d = d + a
    rw [Nat.add_comm]

  -- this all requires a lot more study.
  -- I shall skip it for now.
end tactics

namespace syntaxextensions
  -- infixl:65   " + " => HAdd.hAdd  -- left-associative
  -- infix:50    " = " => Eq         -- non-associative
  -- infixr:80   " ^ " => HPow.hPow  -- right-associative
  -- prefix:100  "-"   => Neg.neg
  -- postfix:max "⁻¹"  => Inv.inv

  --expands to:
  -- notation:65 lhs:65 " + " rhs:66 => HAdd.hAdd lhs rhs
  -- notation:50 lhs:51 " = " rhs:51 => Eq lhs rhs
  -- notation:80 lhs:81 " ^ " rhs:80 => HPow.hPow lhs rhs
  -- notation:100 "-" arg:100 => Neg.neg arg
  -- notation:1024 arg:1024 "⁻¹" => Inv.inv arg  -- `max` is a shorthand for precedence 1024

  -- As mentioned above, the notation command allows us to define arbitrary mixfix syntax freely mixing tokens and placeholders.
  notation:max "(" e ")" => e
  notation:10 Γ " ⊢ " e " : " τ => Typing Γ e τ

  -- Placeholders without precedence default to 0, i.e. they accept notations of any precedence in their place. If two notations overlap, we again apply the longest parse rule:
  notation:65 a " + " b:66 " + " c:66 => a + b - c
  -- #eval 1 + 2 + 3  -- 0 -- does not work

  namespace doNotation
    def main : IO UInt32 := do
      IO.println "hello"
      IO.println "world"
      return 0
    
    -- The do DSL expands into the core Lean language. Let's inspect the different components using the commands #print and #check.

    #check IO.println "hello"
    -- IO Unit
    #print main
    -- Output contains the infix operator `>>=` and `pure`   -- it does not actually
    -- The following `set_option` disables notation such as `>>=` in the output
    set_option pp.notation false in
    #print main
    -- Output contains `bind` and `pure`
    #print bind
    -- bind : {m : Type u → Type v} → [self : Bind m] → {α β : Type u} →
    --        m α → (α → m β) → m β
    #print pure
    -- pure : {m : Type u → Type v} → [self : Pure m] → {α : Type u} →
    --        α → m α

    -- IO implements the type classes `Bind` and `Pure`.
    #check (inferInstance : Bind IO)
    #check (inferInstance : Pure IO)

    -- Here is the same function being defined using bind and pure without the do DSL.
    -- def main' : IO UInt32 :=
    --   bind (IO.println "hello") fun _ =>
    --   bind (IO.println "world") fun _ =>
    --   pure 0

    --The notations let x <- action1; action2 and let x ← action1; action2 are just syntax sugar for bind action1 fun x => action2. Here is a small example using it.

    def isGreaterThan0 (x : Nat) : IO Bool := do
      IO.println s!"value: {x}"
      return x > 0

    def f (x : Nat) : IO Unit := do
      let c <- isGreaterThan0 x
      if c then
        IO.println s!"{x} is greater than 0"
      else
        pure ()

    #eval f 10
    -- value: 10
    -- 10 is greater than 0

    --Note that we cannot write if isGreaterThan0 x then ... else ... because the condition in a if-then-else is a pure value without effects, but isGreaterThan0 x has type IO Bool. You can use the nested action notation to avoid this annoyance. Here is an equivalent definition for f using a nested action.
    -- def f' (x : Nat) : IO Unit := do
    --   if (<- isGreaterThan0 x) then
    --     IO.println s!"{x} is greater than 0"
    --   else
    --     pure ()

    -- #print f'

    -- Lean "lifts" the nested actions and introduces the bind for us. Here is an example with two nested actions. Note that both actions are executed even if x = 0.

    -- def f'' (x y : Nat) : IO Unit := do
    --   if (<- isGreaterThan0 x) && (<- isGreaterThan0 y) then
    --     IO.println s!"{x} and {y} are greater than 0"
    --   else
    --     pure ()

    -- #eval f'' 0 10
    -- value: 0
    -- value: 10

    -- The function `f''` above is equivalent to
    def g (x y : Nat) : IO Unit := do
      let c1 <- isGreaterThan0 x
      let c2 <- isGreaterThan0 y
      if c1 && c2 then
        IO.println s!"{x} and {y} are greater than 0"
      else
        pure ()

    -- theorem fgEqual : f'' = g :=
      -- rfl -- proof by reflexivity
    -- Here are two ways to achieve the short-circuit semantics in the example above



    -- def f1 (x y : Nat) : IO Unit := do
    --   if (<- isGreaterThan0 x <&&> isGreaterThan0 y) then
    --     IO.println s!"{x} and {y} are greater than 0"
    --   else
    --     pure ()

    -- `<&&>` is the effectful version of `&&`
    -- Given `x y : IO Bool`, `x <&&> y` : m Bool`
    -- It only executes `y` if `x` returns `true`.

    -- #eval f1 0 10
    -- value: 0
    -- #eval f1 1 10
    -- value: 1
    -- value: 10
    -- 1 and 10 are greater than 0

    -- def f2 (x y : Nat) : IO Unit := do
    --   if (<- isGreaterThan0 x) then
    --     if (<- isGreaterThan0 y) then
    --       IO.println s!"{x} and {y} are greater than 0"
    --     else
    --       pure ()
    --   else
    --     pure ()

  -- reassignment / variable shadowing
  def f2 (b1 b2 : Bool) : IO Unit := do
    let xs := #[]
    let xs := if b1 then xs.push 0 else xs
    let xs := if b2 then xs.push 1 else xs
    IO.println xs

  #eval f2 true true
  -- #[0, 1]
  #eval f2 false true
  -- #[1]
  #eval f2 true false
  -- #[0]
  #eval f2 false false
  -- #[]

  -- We can use tuples to simulate updates on multiple variables.
  def f3 (b1 b2 : Bool) : IO Unit := do
    let xs := #[]
    let ys := #[]
    let (xs, ys) := if b1 then (xs.push 0, ys) else (xs, ys.push 0)
    let (xs, ys) := if b2 then (xs.push 1, ys) else (xs, ys.push 1)
    IO.println s!"xs: {xs}, ys: {ys}"

  #eval f3 true false
  -- xs: #[0], ys: #[1]

set_option pp.raw true
  -- -- We can also simulate the control-flow above using join-points. A join-point is a let that is always tail called and fully applied. The Lean compiler implements them using gotos. Here is the same example using join-points.
  def f4 (b1 b2 : Bool) : IO Unit := do
    let jp1 xs ys := IO.println s!"xs: {xs}, ys: {ys}"
    let jp2 xs ys := if b2 then jp1 (xs.push 1) ys else jp1 xs (ys.push 1)
    let xs := #[]
    let ys := #[]
    if b1 then jp2 (xs.push 0) ys else jp2 xs (ys.push 0)

  #eval f4 true false
  -- xs: #[0], ys: #[1]
  -- This also DOES NOT WORK!

  -- using mut
  def ff (b1 b2 : Bool) : IO Unit := do
    let mut xs := #[]
    if b1 then xs := xs.push 0
    if b2 then xs := xs.push 1
    IO.println xs

  #eval ff true true
  -- #[0, 1]

  --Iteration
  def sum (xs : Array Nat) : IO Nat := do
    let mut s := 0
    for x in xs do
      IO.println s!"x: {x}"
      s := s.add x
    return s

  #eval sum #[1, 2, 3]
  -- x: 1
  -- x: 2
  -- x: 3
  -- 6


set_option pp.raw true
  -- We can write pure code using the `do` DSL too.
  def sum' (xs : Array Nat) : Nat := do
    let mut s := 0
    for x in xs do
      s := s + x
    return s

  #eval sum' #[1, 2, 3]
  -- 6

set_option pp.raw true
set_option pp.raw.maxDepth 10
  def findNatLessThan (x : Nat) (p : Nat → Bool) : IO Nat := do
    -- [:x] is notation for the range [0, x)
    for i in [:x] do
      if p i then
        return i -- `return` from the `do` block
    throw (IO.userError "value not found")

  #eval findNatLessThan 10 (fun x => x > 5 && x % 4 == 0)
  -- 8


  def sumOddUpTo (xs : List Nat) (threshold : Nat) : IO Nat := do
    let mut s := 0
    for x in xs do
      if x % 2 == 0 then
        continue -- it behaves like the `continue` statement in imperative languages
      IO.println s!"x: {x}"
      -- s := s + x
      s := s.add x
      if s > threshold then
        break -- it behave like the `continue` statement in imperative languages
              -- I don't think it acts like continue.
    IO.println s!"result: {s}"
    return s

  #eval sumOddUpTo [2, 3, 4, 11, 20, 31, 41, 51, 107] 40
  -- x: 3
  -- x: 11
  -- x: 31
  -- result: 45
  -- 45
  end doNotation
  namespace stringinterpolation
    def name := "John"
    def age  := 28

    #eval IO.println s!"Hello, {name}! Are you {age} years old?"

    #eval IO.println ("Hello, " ++ name ++ "! Are you " ++ toString age ++ " years old?")

    -- `println! <interpolated-string>` is a macro for `IO.println s!<interpolated-string>`
    #eval println! "Hello, {name}! Are you {age} years old?"

    def vals := [1, 2, 3]
    #eval IO.println s!"\{ vals := {vals} }"
    #eval IO.println s!"variables: {vals.map (fun i => s!"x_{i}")}"


    -- You can define a ToString instance for your own datatypes.
    structure Person where
      name : String
      age  : Nat

    instance : ToString Person where
      toString : Person -> String
        | { name := n, age := v } => s!"\{ name := {n}, age := {v} }"

    def person1 : Person := {
      name := "John"
      age  := 28
    }

    #eval println! "person1: {person1}"
  end stringinterpolation
end syntaxextensions